import { useNavigate } from "react-router-dom";
import React from "react";

function TodoAdd() {
  const navigate = useNavigate();

  return (
    <div className="todoAdd">
      <button onClick={() => navigate("/add")} className="btn-todoAdd">
        Add New Task
      </button>
    </div>
  );
}

export default TodoAdd;
