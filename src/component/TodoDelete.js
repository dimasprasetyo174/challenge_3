import React from "react";

const TodoDelete = () => {
  return (
    <div className="delete">
      <button className="btnTodoDelete">Delete done task</button>
      <button className="btnTodoDeleteAll">Delete all Task</button>
    </div>
  );
};

export default TodoDelete;
