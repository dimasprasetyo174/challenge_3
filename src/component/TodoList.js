import React from "react";

function TodoList() {
  return (
    <div className="TodoList">
      <div className="btn-todoList">
        <button className="btn-All">All</button>
        <button className="btn-Done">Done</button>
        <button className="btn-Todo">Todo</button>
      </div>
    </div>
  );
}

export default TodoList;
