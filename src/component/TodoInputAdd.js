import { Container, Typography } from "@mui/material";
import { db } from "../firebase";
import React, { useState } from "react";
import ArticleRoundedIcon from "@mui/icons-material/ArticleRounded";
import { addDoc, collection } from "firebase/firestore";

function TodoInputAdd() {
  const [input, setInput] = useState("");

  // Create
  const createTodo = async (e) => {
    e.preventDefault(e);
    if (input === "") {
      alert("please enter a value");
      return;
    }
    await addDoc(collection(db, "todos"), {
      text: input,
      complected: false,
    });
    setInput("");
  };

  return (
    <div>
      <Typography variant="h6" textAlign="center ">
        TodoInput
      </Typography>
      <Container sx={{ border: " 1px ridge rgb(226, 226, 226)", borderRadius: "4px" }}>
        <form onSubmit={createTodo} className="TodoInputbodyAdd">
          <ArticleRoundedIcon className="iconInputAdd" />
          <input value={input} onChange={(e) => setInput(e.target.value)} className="inputAdd" type="text" placeholder=" Input / Edit todo" />
          <button className="btn-todoInputAdd">Submit</button>
        </form>
      </Container>
    </div>
  );
}

export default TodoInputAdd;
