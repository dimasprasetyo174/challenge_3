import SearchRoundedIcon from "@mui/icons-material/SearchRounded";

import React from "react";

function TodoSearch() {
  return (
    <div className="Search">
      <div className="container_search">
        <SearchRoundedIcon className="searchIcon" />
        <input className="inputSearch" type="text" placeholder="Search ..." />
      </div>
      <div>
        <button className="btn-search">Search</button>
      </div>
    </div>
  );
}

export default TodoSearch;
