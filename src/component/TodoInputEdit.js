import { Typography } from "@mui/material";
import React from "react";
import ArticleRoundedIcon from "@mui/icons-material/ArticleRounded";
import { Container } from "@mui/system";

function TodoInputEdit() {
  return (
    <div>
      <Typography variant="h6" textAlign="center ">
        TodoInput
      </Typography>
      <Container sx={{ border: " 1px ridge rgb(226, 226, 226)", borderRadius: "4px" }}>
        <form className="TodoInputbodyEdit">
          <ArticleRoundedIcon className="iconInputEdit" />
          <input className="inputEdit" type="text" placeholder=" Input / Edit todo" />
          <button className="btn-todoInputEdit">Submit</button>
        </form>
      </Container>
    </div>
  );
}

export default TodoInputEdit;
