import React from "react";
import EditRoundedIcon from "@mui/icons-material/EditRounded";
import DeleteOutlinedIcon from "@mui/icons-material/DeleteOutlined";
import { useNavigate } from "react-router-dom";

const Todo = ({ todo, toggleComplete, deleteTodo }) => {
  const navigate = useNavigate();

  return (
    <li className="liTodo">
      <div className="container-todo">
        <p onClick={() => toggleComplete(todo)} className={todo.completed ? "pTodo" : ""}>
          {todo.text}
        </p>
      </div>
      <div className="liBtn">
        <input onChange={() => toggleComplete(todo)} className="checkTodo" type="checkbox" checked={todo.completed ? "checked" : ""} />
        <button onClick={() => navigate("/edit")} className="btnEditTodo">
          <EditRoundedIcon className="iconEditTodo" />
        </button>
        <button onClick={() => deleteTodo(todo.id)} className="btnTrashTodo">
          <DeleteOutlinedIcon className="iconTrashTodo" />
        </button>
      </div>
    </li>
  );
};

export default Todo;
