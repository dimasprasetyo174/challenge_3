import "./App.css";
import Todo from "./component/Todo";
import { Box, Container, Stack, Typography } from "@mui/material";
import TodoSearch from "./component/TodoSearch";
import TodoAdd from "./component/TodoAdd";
import TodoList from "./component/TodoList";
import { useEffect, useState } from "react";
import TodoDelete from "./component/TodoDelete";
import { db } from "./firebase";
import { query, collection, onSnapshot, updateDoc, doc, deleteDoc } from "firebase/firestore";

function Home() {
  const [todos, setTodos] = useState([]);

  // Read

  useEffect(() => {
    const q = query(collection(db, "todos"));
    const unsubscribe = onSnapshot(q, (querySnapshot) => {
      let todosArr = [];
      querySnapshot.forEach((doc) => {
        todosArr.push({ ...doc.data(), id: doc.id });
      });
      setTodos(todosArr);
    });
    return () => unsubscribe();
  });

  // Update

  const toggleComplete = async (todo) => {
    await updateDoc(doc(db, "todos", todo.id), {
      completed: !todo.completed,
    });
  };

  //Delete
  const deleteTodo = async (id) => {
    await deleteDoc(doc(db, "todos", id));
  };

  return (
    <Container>
      <Typography variant="h5" textAlign="center ">
        <strong>TodoSearch</strong>
      </Typography>
      <Box>
        <Stack direction="row" justifyContent="space-between" sx={{ border: " 1px ridge rgb(226, 226, 226)", borderRadius: "4px", margin: "1rem" }}>
          <TodoSearch />
          <TodoAdd />
        </Stack>
      </Box>
      <Box sx={{ margin: "1rem" }}>
        <Typography variant="h6" textAlign="center ">
          TodoList
        </Typography>
        <TodoList />
        <ul>
          {todos.map((todo, index) => (
            <Todo key={index} todo={todo} toggleComplete={toggleComplete} deleteTodo={deleteTodo} />
          ))}
        </ul>
        <TodoDelete />
      </Box>
    </Container>
  );
}

export default Home;
