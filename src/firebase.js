// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAdRefCXaTJ91pMSGj43c3lmtgGHPlu__Y",
  authDomain: "todo-app-yt-66b96.firebaseapp.com",
  projectId: "todo-app-yt-66b96",
  storageBucket: "todo-app-yt-66b96.appspot.com",
  messagingSenderId: "46592939133",
  appId: "1:46592939133:web:be6513002be61895604d14",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
