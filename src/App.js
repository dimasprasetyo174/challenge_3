import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";
import Home from "./Home";
import TodoInputAdd from "./component/TodoInputAdd";
import TodoInputEdit from "./component/TodoInputEdit";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/add" element={<TodoInputAdd />} />
        <Route path="/edit" element={<TodoInputEdit />} />
      </Routes>
    </Router>
  );
}

export default App;
